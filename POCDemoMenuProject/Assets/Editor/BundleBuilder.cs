﻿using UnityEngine;
using UnityEditor;

public class BundleBuilder : Editor {

	[MenuItem("Assets/Build AssetBundles")]
	static void BuildAllAssetBundle()
	{
		//BuildPipeline.BuildAssetBundles("AssetBundles");
		BuildPipeline.BuildAssetBundles( "AssetBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows );
	}
}
